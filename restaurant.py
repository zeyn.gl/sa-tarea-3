import flask
from flask import request, abort, jsonify
import random
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config["PORT"] = 5001


@app.route('/', methods=['GET'])
def home():
    """
    Usado como inicio del proyecto y muestra datos de referencia
    """
    return ""


@app.route('/api/v1.0/restaurant/order/', methods=['POST'])
def restaurant_order():
    """
    Servicio que maneja ordenes de un restaurante
    """
    required = ['product_id', 'quantity']
    if not all(term in required for term in request.json):
        abort(400)
    order = {
        'message': 'Orden tomada por restaurante',
        'success': True
    }
    return jsonify(order), 201

app.run(port=5001)