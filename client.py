import flask
from flask import request, abort, jsonify
import random
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    """
    Usado como inicio del proyecto y muestra datos de referencia
    """
    return ""


@app.route('/api/v1.0/client/order/', methods=['POST'])
def client_order():
    """
    Servicio que toma la orden de un cliente para un producto determinado
    """
    required = ['client_id', 'product_id', 'quantity', 'address', 'phone']
    if not all(term in required for term in request.json):
        abort(400)
    order = {
        'order_id': random.randint(1, 1000),
        'message': 'Orden creada correctamente',
        'success': True
    }
    return jsonify({'order': order}), 201


app.run(port=5003)