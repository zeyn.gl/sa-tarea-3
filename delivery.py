import flask
from flask import request, abort, jsonify
import random
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    """
    Usado como inicio del proyecto y muestra datos de referencia
    """
    return ""


@app.route('/api/v1.0/delivery/order/', methods=['POST'])
def delivery_order():
    """
    Servicio que maneja ordenes de un restaurante
    """
    required = ['order_id', 'status', 'signature']
    status = ['delivered', 'not_delivered', 'issue']

    if not all(term in required for term in request.json):
        abort(400)
    if not request.json['status'] in status:
        """ Valida que el estado se encuentre dentro de los aceptados"""

        order = {
            'message': 'Estado invalido',
            'success': False
        }
        return jsonify({'order': order}), 400
    if not int(request.json['order_id']) <= 1000:
        """ Valida que la orden exista dentro de las tomadas anteriormente"""

        order = {
            'message': 'Orden no encontrada',
            'success': False
        }
        return jsonify({'order': order}), 400
    order = {
        'message': 'Orden actualizada correctamente',
        'success': True
    }
    return jsonify(order), 202

app.run(port=5002)