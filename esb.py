import flask
from flask import request, abort, jsonify
import random
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True

restaurant_ws = 'http://127.0.0.1:5001'
delivery_ws = 'http://127.0.0.1:5002'
client_ws = 'http://127.0.0.1:5003'

@app.route('/', methods=['GET'])
def home():
    """
    Usado como inicio del proyecto y muestra datos de referencia
    """
    return ""


@app.route('/api/v1.0/esb/functions/', methods=['POST'])
def esb_orchestrator():
    """
    Servicio que administra y orquesta los webservice recibidos medianto POST
    """
    required = ['service']
    services_available = ['create_order', 'delivery_order', 'restaurant_order', 'delivery_assignation']
    if not all(term in required for term in request.json) and not request.json['service'] in services_available:
        abort(400)

    service_name = request.json.pop('service')
    required_service = []
    complete_url = ""

    if service_name == 'create_order':
        required_service = ['client_id', 'product_id', 'quantity', 'address', 'phone']
        complete_url = client_ws + '/api/v1.0/client/order/'

    elif service_name == 'delivery_order':
        required_service = ['order_id', 'status', 'signature']
        complete_url = delivery_ws + '/api/v1.0/delivery/order/'


    elif service_name == 'restaurant_order':
        required_service = ['product_id', 'quantity']
        complete_url = restaurant_ws + '/api/v1.0/restaurant/order/'

    else:
        abort(400)

    if not all(terms in required_service for terms in request.json):
        abort(400)

    response = requests.post(complete_url, json=request.json)
    return response.content, response.status_code


app.run(port=6000)