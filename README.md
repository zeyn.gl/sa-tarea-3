Universidad de San Carlos de Guatemala
=========

Esta es una tarea realizada en el curso de Software Avanzado de la carrera de Ingenieria
en Ciencias y Sistemas para comprender la publicacion de API
aplicando conceptos basicos de SOA

Para poder utilizar los servicios se deben de ejecutar de la siguiente forma cada uno de los archivos:
```
python run esb.py
```
Cada servicio se ejecutara en diferente puerto:

* ESB : Puerto 6000
* Restaurante : Puerto 5001
* Delivery : Puerto 5002
* Cliente : Puerto 5003


Funciones que se pueden realizar dentro del software:
* Un cliente puede crear una orden

``` 
curl -i -H "Content-Type: application/json" -X POST -d '{"client_id":"1", "product_id":"544", "quantity": "2", "address":"Ciudad Guatemala", "phone":"12345678"}' http://127.0.0.1:5000/api/v1.0/client/order/
```

* El restaurante recibe la orden

``` 
curl -i -H "Content-Type: application/json" -X POST -d '{"product_id":"544", "quantity": "2"}' http://127.0.0.1:5000/api/v1.0/restaurant/order/
 ```

* Al entregar el motorista puede actualizar el estado de la orden
```
curl -i -H "Content-Type: application/json" -X UPDATE -d '{"order_id":"544", "status": "delivered", "signature":"Giovanni Lopez"}' http://127.0.0.1:5000/api/v1.0/delivery/order/
```


## ESB (Enterprise service bus)

Se utiliza realizando la consulta al puerto 6000 que pertenece al ESB y se adjunta
parametro llamado *service* que es el encargado de indicar cual servicio se desea consultar y
el mismo se encarga de redirigirlo al servidor correspondiente

Los posibles nombres de servicio son:
* create_order
* delivery_order
* restaurant_order

Por ejemplo:
```
curl -i -H "Content-Type: application/json" -X POST -d '{"service":"create_order","client_id":"1", "product_id":"544", "quantity": "2", "address":"Ciudad Guatemala", "phone":"12345678"}' http://127.0.0.1:6000/api/v1.0/esb/functions/
```

```
curl -i -H "Content-Type: application/json" -X POST -d '{"service":"delivery_order","order_id":"544", "status": "delivered", "signature":"Giovanni Lopez"}' http://127.0.0.1:6000/api/v1.0/esb/functions/
```
```
curl -i -H "Content-Type: application/json" -X POST -d '{"service":"restaurant_order","product_id":"544", "quantity": "2"}' http://127.0.0.1:6000/api/v1.0/esb/functions/
```



`Esta es una prueba de concepto y no se puede tomar
 en cuenta para evaluaciones de seguridad ni produccion`
 <br>
 <br>
_Fredy Giovanni Lopez Ordoñez_ <br>
_2007-22339_

